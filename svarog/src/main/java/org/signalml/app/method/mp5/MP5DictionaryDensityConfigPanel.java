/* MP5DictionaryDensityConfigPanel.java created 2008-01-30
 *
 */
package org.signalml.app.method.mp5;

import java.awt.Component;
import java.awt.GridLayout;
import javax.swing.Box;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import org.signalml.app.model.components.validation.ValidationErrors;
import static org.signalml.app.util.i18n.SvarogI18n._;
import org.signalml.app.view.common.dialogs.AbstractDialog;
import org.signalml.method.mp5.MP5Parameters;

/** MP5DictionaryDensityConfigPanel
 *
 *
 * @author Michal Dobaczewski &copy; 2007-2008 CC Otwarte Systemy Komputerowe Sp. z o.o.
 */
public class MP5DictionaryDensityConfigPanel extends JPanel {

	private static final long serialVersionUID = 2L;
	private AbstractDialog owner;

	private MP5DictionaryTypeConfigPanel dictionaryTypeConfigPanel;
	private JSpinner energyErrorSpinner;
	private JTextField atomCountTextField;

	public MP5DictionaryDensityConfigPanel(AbstractDialog owner) {
		super();
		this.owner = owner;
		initialize();
	}

	private void initialize() {

		CompoundBorder border = new CompoundBorder(
			new TitledBorder(_("Dictionary density and size")),
			new EmptyBorder(3,3,3,3)
		);

		setBorder(border);

		GridLayout layout = new GridLayout(3, 2);
		this.setLayout(layout);

		JLabel dictionaryTypeLabel = new JLabel(_("Dictionary type"));
		JLabel energyErrorLabel = new JLabel(_("Energy error"));
		JLabel atomCountLabel = new JLabel(_("Atoms in dictionary"));

		add(dictionaryTypeLabel);
		add(getDictionaryTypeConfigPanel());

		add(energyErrorLabel);
		add(getEnergyErrorSpinner());

		add(atomCountLabel);
		add(getAtomCountTextField());
	}

	@SuppressWarnings("cast")
	public JSpinner getEnergyErrorSpinner() {
		if (energyErrorSpinner == null) {
			energyErrorSpinner = new JSpinner(
				new SpinnerNumberModel(
					((double) MP5Parameters.MIN_ENERGY_ERROR),
					((double) MP5Parameters.MIN_ENERGY_ERROR),
					((double) MP5Parameters.MAX_ENERGY_ERROR),
					0.01d
				)
			);
			energyErrorSpinner.setPreferredSize(MP5MethodDialog.FIELD_SIZE);
			energyErrorSpinner.setMaximumSize(MP5MethodDialog.FIELD_SIZE);
			energyErrorSpinner.setMinimumSize(MP5MethodDialog.FIELD_SIZE);
		}
		return energyErrorSpinner;
	}

	public JTextField getAtomCountTextField() {
		if (atomCountTextField == null) {
			atomCountTextField = new JTextField();
			atomCountTextField.setPreferredSize(MP5MethodDialog.FIELD_SIZE);
			atomCountTextField.setMaximumSize(MP5MethodDialog.FIELD_SIZE);
			atomCountTextField.setMinimumSize(MP5MethodDialog.FIELD_SIZE);
			atomCountTextField.setHorizontalAlignment(JTextField.RIGHT);
			atomCountTextField.setEditable(false);
		}
		return atomCountTextField;
	}

	public MP5DictionaryTypeConfigPanel getDictionaryTypeConfigPanel() {
		if (dictionaryTypeConfigPanel == null) {
			dictionaryTypeConfigPanel = new MP5DictionaryTypeConfigPanel();
		}
		return dictionaryTypeConfigPanel;
	}

	public void fillPanelFromParameters(MP5Parameters parameters) {

		getDictionaryTypeConfigPanel().fillPanelFromModel(parameters);
		getEnergyErrorSpinner().setValue(new Double(parameters.getEnergyError()));

	}

	public void fillParametersFromPanel(MP5Parameters parameters) {

		getDictionaryTypeConfigPanel().fillModelFromPanel(parameters);
		parameters.setEnergyError(((Number) getEnergyErrorSpinner().getValue()).floatValue());

	}

	public void validatePanel(ValidationErrors errors) {

		// nothing to do

	}

}
