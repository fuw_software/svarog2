package org.signalml.app.method.mp5;

/**
 * Mode of optimization of atom parameters in parameter space used by empi.
 * Corresponds to the discrete vs continuous dictionary issue.
 *
 * @author PTR
 */
public enum ParameterOptimizationMode {

	/**
	 * For NONE, parameters are taken directly from the discrete dictionary
	 * without any local optimization.
	 */
	NONE,

	/**
	 * For LOCAL, parameter optimization is performed
	 * only starting from the best match in each iteration.
	 */
	LOCAL,

	/**
	 * For GLOBAL, parameter optimization is performed
	 * starting from each potential candidate for a global best match,
	 * resulting in simulating a continuous dictionary.
	 */
	GLOBAL,
}
