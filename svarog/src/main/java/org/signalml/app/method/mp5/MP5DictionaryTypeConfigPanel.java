package org.signalml.app.method.mp5;

import java.awt.Dimension;
import org.signalml.app.view.signal.export.*;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import org.apache.log4j.Logger;
import static org.signalml.app.util.i18n.SvarogI18n._;
import org.signalml.method.mp5.MP5Parameters;

/**
 * Small sub-panel for choosing between discrete and continuous (simulated) dictionary.
 *
 * @author PTR
 */
public class MP5DictionaryTypeConfigPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	protected static final Logger logger = Logger.getLogger(MP5DictionaryTypeConfigPanel.class);

	private Map<ParameterOptimizationMode, JRadioButton> buttons;
	private ButtonGroup buttonGroup;

	public MP5DictionaryTypeConfigPanel() {
		initialize();
	}

	private void initialize() {

		buttons = new HashMap<>();
		buttonGroup = new ButtonGroup();

		addButton(ParameterOptimizationMode.NONE, _("discrete"));
		addButton(ParameterOptimizationMode.LOCAL, _("continuous (approx.)"));
		addButton(ParameterOptimizationMode.GLOBAL, _("continuous"));

		setPreferredSize(new Dimension(0, 0));
	}

	private void addButton(ParameterOptimizationMode value, String text) {
		JRadioButton button = new JRadioButton(text);
		buttonGroup.add(button);
		buttons.put(value, button);
		add(button);
	}

	public void fillPanelFromModel(MP5Parameters parameters) {
		ParameterOptimizationMode mode = parameters.getParameterOptimizationMode();
		buttons.forEach((buttonMode, button) -> {
			button.setSelected(buttonMode == mode);
		});
	}

	public void fillModelFromPanel(MP5Parameters parameters) {
		buttons.forEach((buttonMode, button) -> {
			if (button.isSelected()) {
				parameters.setParameterOptimizationMode(buttonMode);
			}
		});
	}
}