/* MP5LocalProcessExecutor.java created 2008-02-08
 *
 */

package org.signalml.method.mp5;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import org.apache.log4j.Logger;
import org.signalml.app.model.signal.SignalExportDescriptor;
import static org.signalml.app.util.i18n.SvarogI18n._;
import org.signalml.domain.signal.raw.RawSignalByteOrder;
import org.signalml.domain.signal.raw.RawSignalSampleType;
import org.signalml.domain.signal.raw.RawSignalWriter;
import org.signalml.domain.signal.samplesource.MultichannelSegmentedSampleSource;
import org.signalml.method.ComputationException;
import org.signalml.method.MethodExecutionTracker;

/** MP5LocalProcessExecutor
 *
 *
 * @author Michal Dobaczewski &copy; 2007-2008 CC Otwarte Systemy Komputerowe Sp. z o.o.
 */
@XStreamAlias("mp5localexecutor")
public class MP5LocalProcessExecutor implements MP5Executor {

	protected static final Logger logger = Logger.getLogger(MP5LocalProcessExecutor.class);

	private static final String[] CODES = new String[] { "mp5Method.executor.localProcess" };

	private String uid;

	private String name;
	protected String mp5ExecutablePath;

	private transient RawSignalWriter rawSignalWriter = new RawSignalWriter();
	private transient MP5LocalProcessController processController = new MP5LocalProcessController();

	public MP5LocalProcessExecutor() {
		uid = UUID.randomUUID().toString();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getUID() {
		return uid;
	}

	public String getMp5ExecutablePath() {
		return mp5ExecutablePath;
	}

	public void setMp5ExecutablePath(String mp5ExecutablePath) {
		this.mp5ExecutablePath = mp5ExecutablePath;
	}

	@Override
	public boolean execute(MP5Data data, File resultFile, MethodExecutionTracker tracker) throws ComputationException {

		File workingDirectory = data.getWorkingDirectory();

		MP5Parameters parameters = data.getParameters();
		MultichannelSegmentedSampleSource sampleSource = data.getSampleSource();
		final int totalSegmentCount = sampleSource.getSegmentCount();

		File signalFile = new File(workingDirectory, "signal.bin");

		MP5RuntimeParameters runtimeParameters = new MP5RuntimeParameters();

		runtimeParameters.setMinOffset(1);
		runtimeParameters.setMaxOffset(totalSegmentCount);
		runtimeParameters.setChannelCount(sampleSource.getChannelCount());
		runtimeParameters.setSegmentSize(sampleSource.getSegmentLengthInSamples());
		runtimeParameters.setPointsPerMicrovolt(1F);
		runtimeParameters.setSamplingFrequency(sampleSource.getSamplingFrequency());
		runtimeParameters.setSignalFile(signalFile);
		runtimeParameters.setWritingMode(MP5WritingModeType.CREATE);

		EmpiCommandLineBuilder commandLineBuilder = new EmpiCommandLineBuilder(runtimeParameters, parameters);

		SignalExportDescriptor signalExportDescriptor = new SignalExportDescriptor();
		signalExportDescriptor.setSampleType(RawSignalSampleType.FLOAT);
		signalExportDescriptor.setByteOrder(RawSignalByteOrder.LITTLE_ENDIAN);
		signalExportDescriptor.setNormalize(false);

		tracker.setMessage(_("Writing signal file"));

		// write data file
		try {
			for (int segment=0; segment<totalSegmentCount; ++segment) {
				rawSignalWriter.setAppendMode(segment > 0);
				rawSignalWriter.writeSignal(signalFile, sampleSource, signalExportDescriptor, segment, null);
			}
		} catch (IOException ex) {
			logger.error("Failed to create data file", ex);
			throw new ComputationException(ex);
		}

		// delete results in the way
		File generatedBookFile;
		if (parameters.getAlgorithm() == MP5Algorithm.SMP) {
			generatedBookFile = new File(workingDirectory, "signal_smp.db");
		} else {
			generatedBookFile = new File(workingDirectory, "signal_mmp.db");
		}
		runtimeParameters.setOutputBookPath(generatedBookFile);
		if (generatedBookFile.exists()) {
			generatedBookFile.delete();
		}

		tracker.setMessage(_("Starting executable"));

		boolean executionOk = processController.executeProcess(workingDirectory, getMp5ExecutablePath(), commandLineBuilder, tracker);
		if (!executionOk) {
			return false;
		}

		if (!generatedBookFile.exists()) {
			logger.error("MP process didn't produce expected result file [" + generatedBookFile.getAbsolutePath() + "]");
			throw new ComputationException("error.mp5.exitedWithNoResult");
		}

		boolean renameOk = generatedBookFile.renameTo(resultFile);
		if (!renameOk) {
			logger.error("Failed to rename [" + generatedBookFile.getAbsolutePath() + "] to [" + resultFile.getAbsolutePath() + "]");
			throw new ComputationException("error.mp5.resultRenameFailed");
		}

		return true;

	}

	@Override
	public Object[] getArguments() {
		return new Object[] { name };
	}

	@Override
	public String[] getCodes() {
		return CODES;
	}

	@Override
	public String getDefaultMessage() {
		return "MP5LocalProcessExecutor [" + name + "]";
	}

	@Override
	public String toString() {
		return getDefaultMessage();
	}

	public static MP5LocalProcessExecutor pathExecutor() {
		MP5LocalProcessExecutor ex = new MP5LocalProcessExecutor();
		ex.setName(_("execute from $PATH"));
		ex.setMp5ExecutablePath("empi");
		return ex;
	}
}
