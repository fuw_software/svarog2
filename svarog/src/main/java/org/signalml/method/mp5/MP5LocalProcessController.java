/* MP5LocalProcessController.java created 2008-02-18
 *
 */

package org.signalml.method.mp5;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import static org.signalml.app.util.i18n.SvarogI18n._;
import org.signalml.method.ComputationException;
import org.signalml.method.MethodExecutionTracker;

/** MP5LocalProcessController
 *
 *
 * @author Michal Dobaczewski &copy; 2007-2008 CC Otwarte Systemy Komputerowe Sp. z o.o.
 */
public class MP5LocalProcessController {

	protected static final Logger logger = Logger.getLogger(MP5LocalProcessController.class);

	private static final String ERROR = "ERROR";
	private static final Pattern PROGRESS_PATTERN = Pattern.compile("([0-9]+)%");

	public boolean executeProcess(File workingDirectory, String mp5ExecutablePath, EmpiCommandLineBuilder commandLineBuilder, MethodExecutionTracker tracker) throws ComputationException {

		ProcessBuilder pb = new ProcessBuilder();
		List<String> args = commandLineBuilder.buildCommandLine();
		args.add(0, mp5ExecutablePath);
		pb.command(args);
		pb.directory(workingDirectory);
		pb.redirectErrorStream(true);

		logger.debug("Using MP working directory [" + pb.directory().getAbsolutePath() + "]");
		List<String> command = pb.command();
		for (String s : command) {
			logger.debug("Using MP command [" + s + "]");
		}

		Process process;
		try {
			process = pb.start();
			logger.debug("Process started");
		} catch (IOException ex) {
			throw new ComputationException(ex);
		}

		tracker.setMessage(_("Calculating"));

		BufferedReader feedbackReader = null;
		try {

			feedbackReader = new BufferedReader(new InputStreamReader(process.getInputStream()), 1);
			String line;
			boolean msgUnderstood;

			do {
				line = feedbackReader.readLine(); // XXX this may prevent prompt abort, rethink
				logger.debug("Process returned line [" + (line != null ? line : "(null)") + "]");
				if (line != null && !line.isEmpty()) {
					line = line.trim();
					if (!line.isEmpty()) {
						try {
							msgUnderstood = processMessage(line, tracker);
						} catch (ComputationException ex) {
							logger.debug("Execution exception");
							process.destroy();
							try {
								process.waitFor();
							} catch (InterruptedException ex2) {
								// ignore
							}
							logger.debug("Process terminated");
							throw ex;
						}
						if (!msgUnderstood) {
							logger.warn("Message line not understood [" + line + "]");
						}
					}
				}
				if (tracker.isRequestingAbort() || tracker.isRequestingSuspend()) {
					logger.debug("Termination request received");
					process.destroy();
					try {
						process.waitFor();
					} catch (InterruptedException ex2) {
						// ignore
					}
					logger.debug("Process terminated");
					return false;
				}
			} while (line != null);

		} catch (IOException ex) {
			logger.error("IOException reading from report stream", ex);
			throw new ComputationException(ex);
		} finally {
			if (feedbackReader != null) {
				try {
					feedbackReader.close();
				} catch (IOException ex) {
				}
			}
		}

		try {
			process.waitFor();
		} catch (InterruptedException e) {
			// ignore
		}

		// end of the process (should we ignore the last message)
		int exitValue = process.exitValue();
		logger.debug("Process may have finished, exit code is [" + exitValue + "]");
		if (exitValue != 0) {
			logger.warn("MP process exited with an error [" + exitValue + "]");
			throw new ComputationException("error.mp5.exitedWithErrorCode", new Object[] { exitValue });
		}

		return true;

	}

	private boolean processMessage(String line, MethodExecutionTracker tracker) throws ComputationException {

		if (line.startsWith(ERROR)) {

			String[] parts = line.split("\\s+");
			if (parts.length < 2) {
				return false;
			}

			String[] args = Arrays.copyOfRange(parts, 2, parts.length);

			throw new ComputationException(parts[1], args);

		}

		Matcher matcher;

		matcher = PROGRESS_PATTERN.matcher(line);
		if (matcher.lookingAt()) {

			int progress = Integer.parseInt(matcher.group(1));

			synchronized (tracker) {
				tracker.setTicker(0, progress);
			}

			return true;

		}

		return false;

	}

}
