package org.signalml.method.mp5;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple container for command line parameters passed to empi.
 * The usage pattern is to create an empty object, call add() method
 * one or more times, and then get the parameters as a list with get()
 *
 * @author PTR
 */
public class EmpiCommandLine {

	private final ArrayList<String> args;

	/**
	 * Create an empty container with no parameters.
	 */
	public EmpiCommandLine() {
		args = new ArrayList<>();
	}

	/**
	 * Add a single command line parameter.
	 *
	 * @param flag parameter or flag, e.g. "--gabor"
	 */
	public void add(String flag) {
		args.add(flag);
	}

	/**
	 * Add a single command line parameter followed by an integer argument.
	 *
	 * @param flag parameter or flag, e.g. "--channel-count"
	 * @param arg integer argument, e.g. 20
	 */
	public void add(String flag, int arg) {
		args.add(flag);
		args.add(Integer.toString(arg));
	}

	/**
	 * Add a single command line parameter followed by a floating-point argument.
	 *
	 * @param flag parameter or flag, e.g. "--energy-error"
	 * @param arg floating-point argument, e.g. 0.1
	 */
	public void add(String flag, float arg) {
		args.add(flag);
		args.add(Float.toString(arg));
	}

	/**
	 * Add a single command line parameter followed by a text argument.
	 *
	 * @param flag parameter or flag, e.g. "--channels"
	 * @param arg text argument, e.g. "1-5"
	 */
	public void add(String flag, String arg) {
		args.add(flag);
		args.add(arg);
	}

	/**
	 * Get list of all arguments as text values.
	 * The returned list will be a new object,
	 * not associated with the internal state of EmpiCommandLine.
	 *
	 * @return list of all command-line arguments
	 * e.g. ["--energy-error", "0.1", "--channels", "1-5"]
	 */
	public List<String> get() {
		return new ArrayList(args);
	}
}
