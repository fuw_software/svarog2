package org.signalml.method.mp5;

import java.util.List;
import static org.signalml.method.mp5.MP5Algorithm.MMP1;
import static org.signalml.method.mp5.MP5Algorithm.MMP3;

/**
 * Builds command line for empi based on Svarog’s internal parameters.
 *
 * @author PTR
 */
public class EmpiCommandLineBuilder {

	private static final String CHANNEL_COUNT = "-c";
	private static final String DELTA_ATOMS = "--delta";
	private static final String ENERGY_ERROR = "--energy-error";
	private static final String GABOR_ATOMS = "--gabor";
	private static final String MAX_ENERGY_RESIDUAL = "-r";
	private static final String MAX_ITERATION_COUNT = "-i";
	private static final String MMP1_FLAG = "--mmp1";
	private static final String MMP3_FLAG = "--mmp3";
	private static final String OPTIMIZATION_MODE = "-o";
	private static final String SAMPLING_FREQUENCY = "-f";
	private static final String SEGMENT_RANGE = "--segments";
	private static final String SEGMENT_SIZE = "--segment-size";

	private final MP5RuntimeParameters runtimeParameters;
	private final MP5Parameters staticParameters;

	/**
	 * Create a new builder based on two sets of parameters.
	 * Each set of parameters may be omitted (passed as null instead).
	 *
	 * @param runtimeParameters (optional) set of runtime parameters e.g. file paths
	 * @param staticParameters (optional) set of static parameters e.g. decomposition settings
	 */
	public EmpiCommandLineBuilder(MP5RuntimeParameters runtimeParameters, MP5Parameters staticParameters) {
		this.runtimeParameters = runtimeParameters;
		this.staticParameters = staticParameters;
	}

	/**
	 * Get a list of all command-line parameters.
	 * The returned list will be a new object,
	 * not associated with the internal state of EmpiCommandLineBuilder.
	 *
	 * @return list of all command-line arguments
	 * e.g. ["--energy-error", "0.1", "--channels", "1-5"]
	 */
	public List<String> buildCommandLine() {
		EmpiCommandLine cmd = new EmpiCommandLine();
		if (runtimeParameters != null) {
			addRuntimeParameters(cmd, runtimeParameters);
		}
		if (staticParameters != null) {
			addStaticParameters(cmd, staticParameters);
		}
		return cmd.get();
	}

	/**
	 * Get a combined text representation of all command-line parameters.
	 * Parameters will be space-separated and no further escaping will be performed.
	 * Therefore, it may not work correctly for text parameters including spaces.
	 *
	 * @return text representation of all command-line arguments
	 * e.g. "--energy-error 0.1 --channels 1-5"
	 */
	public String buildCommandLineAsText() {
		return String.join(" ", buildCommandLine());
	}

	/**
	 * Compose and add representation of runtime parameters for empi.
	 *
	 * @param cmd command line object to add parameters to
	 * @param params runtime parameter set
	 */
	private static void addRuntimeParameters(EmpiCommandLine cmd, MP5RuntimeParameters params) {
		cmd.add(params.getSignalFile().getName());
		cmd.add(params.getOutputBookPath().getName());

		cmd.add(CHANNEL_COUNT, params.getChannelCount());

		cmd.add(SAMPLING_FREQUENCY, params.getSamplingFrequency());

		cmd.add(SEGMENT_RANGE, composeSegmentRange(params.getMinOffset(), params.getMaxOffset()));
		cmd.add(SEGMENT_SIZE, params.getSegmentSize());
	}

	/**
	 * Compose and add representation of static parameters for empi.
	 *
	 * @param cmd command line object to add parameters to
	 * @param params static parameter set
	 */
	private static void addStaticParameters(EmpiCommandLine cmd, MP5Parameters params) {
		if (params.getRawConfigText() != null) {
			for (String param : params.getRawConfigText().split("\\s+")) {
				if (!param.isEmpty()) {
					cmd.add(param);
				}
			}
			return; // if raw config text is set, it overrides all other options
		}

		cmd.add(OPTIMIZATION_MODE,  params.getParameterOptimizationMode().toString().toLowerCase());

		AtomsInDictionary atoms = params.getAtomsInDictionary();
		if (atoms.isAtomIncluded(MP5AtomType.DIRAC)) {
			cmd.add(DELTA_ATOMS);
		}
		if (atoms.isAtomIncluded(MP5AtomType.GABOR)) {
			cmd.add(GABOR_ATOMS);
		}

		cmd.add(ENERGY_ERROR, params.getEnergyError());

		// this non-zero epsilon is required as empi 1.0.0 does not accept -r 0
		float residualEnergy = Math.max(1e-6f, 1.0f - 0.01f * params.getEnergyPercent());
		cmd.add(MAX_ENERGY_RESIDUAL, residualEnergy);
		if (params.getMaxIterationCount() != Integer.MAX_VALUE) {
			cmd.add(MAX_ITERATION_COUNT, params.getMaxIterationCount());
		}

		switch (params.getAlgorithm()) {
			case MMP1:
				cmd.add(MMP1_FLAG);
				break;
			case MMP3:
				cmd.add(MMP3_FLAG);
				break;
		}

		for (String param : params.getCustomConfigText().split("\\s+")) {
			if (!param.isEmpty()) {
				cmd.add(param);
			}
		}
	}

	/**
	 * Compose text representation of the integer range, e.g. "1-5" or "3".
	 *
	 * @param minOffset e.g. "1"
	 * @param maxOffset e.g. "5"
	 * @return "1-5"
	 */
	private static String composeSegmentRange(int minOffset, int maxOffset) {
		if (minOffset < 0 || maxOffset < 0) {
			return "1";
		}
		if (minOffset == maxOffset) {
			return Integer.toString(minOffset);
		}
		return String.format("%d-%d", minOffset, maxOffset);
	}
}
