/* MP5RuntimeParameters.java created 2008-01-31
 *
 */

package org.signalml.method.mp5;

import java.io.File;

/** MP5RuntimeParameters
 *
 *
 * @author Michal Dobaczewski &copy; 2007-2008 CC Otwarte Systemy Komputerowe Sp. z o.o.
 */
public class MP5RuntimeParameters {

	private File signalFile;
	private File outputBookPath;
	private MP5WritingModeType writingMode = MP5WritingModeType.CREATE;
	private int channelCount = 1;
	private int minOffset = -1;
	private int maxOffset = -1;
	private int segmentSize;
	private float samplingFrequency = 128F;
	private float pointsPerMicrovolt = 1F;

	public File getSignalFile() {
		return signalFile;
	}

	public void setSignalFile(File signalFile) {
		this.signalFile = signalFile;
	}

	public File getOutputBookPath() {
		return outputBookPath;
	}

	public void setOutputBookPath(File outputBookPath) {
		this.outputBookPath = outputBookPath;
	}

	public MP5WritingModeType getWritingMode() {
		return writingMode;
	}

	public void setWritingMode(MP5WritingModeType writingMode) {
		this.writingMode = writingMode;
	}

	public int getChannelCount() {
		return channelCount;
	}

	public void setChannelCount(int channelCount) {
		this.channelCount = channelCount;
	}

	public int getSegmentSize() {
		return segmentSize;
	}

	public void setSegmentSize(int segmentSize) {
		this.segmentSize = segmentSize;
	}

	public float getSamplingFrequency() {
		return samplingFrequency;
	}

	public void setSamplingFrequency(float samplingFrequency) {
		this.samplingFrequency = samplingFrequency;
	}

	public float getPointsPerMicrovolt() {
		return pointsPerMicrovolt;
	}

	public void setPointsPerMicrovolt(float pointsPerMicrovolt) {
		this.pointsPerMicrovolt = pointsPerMicrovolt;
	}

	public int getMinOffset() {
		return minOffset;
	}

	public void setMinOffset(int minOffset) {
		this.minOffset = minOffset;
	}

	public int getMaxOffset() {
		return maxOffset;
	}

	public void setMaxOffset(int maxOffset) {
		this.maxOffset = maxOffset;
	}

}
